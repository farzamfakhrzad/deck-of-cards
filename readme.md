# Deck of Cards (Three.js Journey)
## Peremise 
as part of my journey to understand the fundementals of THREE.js i will try to create a deck of cards in my practice space
## Setup
Download [Node.js](https://nodejs.org/en/download/).
Run this followed commands:

``` bash
# Install dependencies (only the first time)
yarn install

# Run the local server 
vite dev

# Build for production in the dist/ directory
vite  build
```
